﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
namespace Middle_3_events
{
   
   public class WaitTo
    {
       private int _incValue;
        public WaitTo()
        {
            WaitEventReached += new WaitEventHandler(oCounter_NumberReached);
          
        }


        public delegate void WaitEventHandler(object sender,IncEvent e);



        public event WaitEventHandler WaitEventReached;
       

          


        public void Wait(int IncValue)
        {
           

            if (IncValue>32000)
              throw new ArgumentOutOfRangeException();
            Console.WriteLine("Prepare and wait...");
            Timer waitTo = new Timer();
            _incValue=IncValue;
            waitTo.Elapsed+=new ElapsedEventHandler(timerEllapsed);
            waitTo.Interval = 10000;
            waitTo.Start();


    }

       private void timerEllapsed(object sender,ElapsedEventArgs z)
       {
          IncEvent e = new IncEvent(_incValue);
       OnNumberReached(e);
       }
          


        public  void OnNumberReached(IncEvent e)
        {
            

            if (WaitEventReached != null)
            {

                WaitEventReached(this, e);//Raise the event
             
            }

        }
        
       public void oCounter_NumberReached(object sender, IncEvent e)

{
    DateTime now =new DateTime();
    now = DateTime.Now;

           Console.WriteLine("-> Event Reached, now: " + now);

}
        
         
    }
}
