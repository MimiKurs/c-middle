﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace _1_middle_task_Xml_serial_and_Types
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
            Student Mishka = new Student()
            {
                FirstName = "Innokentiy",
                LastName = "Best",
                Age = 15
            };
             */


            MyGroup ak11_1 = new MyGroup();

            ak11_1.Add(new Student  {
                FirstName = "Innokentiy",
                LastName = "Best",
                Age = 15
            });
            ak11_1.Add(new Student
            {
                FirstName = "Vasya",
                LastName = "Pupkin",
                Age = 2048
            });
            ak11_1.Add(new Student
            {
                FirstName = "direct",
                LastName = "X",
                Age = 18
            });
            string way = "xmlSerialaze.txt";
            string way1 = "binarySerialaze.txt";
          



            //serialize binary data
            Tofile.To(way1,true, ak11_1);

            //serialize xml data
            Tofile.To(way, false, ak11_1);


            //create empty class for data
            MyGroup binaryTest = new MyGroup();
            MyGroup xmlTest = new MyGroup();

            //deserialize binary data 
            Tofile.Out(way1, true,ref binaryTest);

            //deserialize xml data 
            Tofile.Out(way, false,ref xmlTest);

            foreach (var a in ak11_1)
            {
                Console.WriteLine(a.ToString());
            }



            Console.WriteLine("\n\n\n\n\n Binary test \n");
            
            foreach (var b in binaryTest)
            {
                Console.WriteLine(b.ToString());
            }
           
            Console.WriteLine("\n\n\n\n\n xml test \n");
            foreach (var c in xmlTest)
            {
                Console.WriteLine(c.ToString());
            }
             
            Console.ReadLine();

        }
    }
}
