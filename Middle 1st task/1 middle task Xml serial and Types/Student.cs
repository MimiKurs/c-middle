﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace _1_middle_task_Xml_serial_and_Types
{
                                 [Serializable]
    public class Student
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int Age { get; set; }

        public override string ToString()
        {
            return string.Format("name {0} surname {1} age {2}",FirstName,LastName,Age);
        }
    }
}
