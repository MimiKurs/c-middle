﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.Specialized;
using System.Collections;


namespace _1_middle_task_Xml_serial_and_Types
{                                                                       [Serializable]
    public class MyGroup : IList<Student>
    {
        private List<Student> _list = new List<Student>();



        public int IndexOf(Student item)
        {
            return _list.IndexOf(item);
        }

        public void Insert(int index, Student item)
        {
             _list.Insert(index,item);
        }

        public void RemoveAt(int index)
        {
            _list.RemoveAt(index);
        }

        public Student this[int index]
        {
            get
            {
                if(index<=_list.Count&&index>=0)
                    return _list[index];
                else 
                    throw new IndexOutOfRangeException();
            }
            set
            {
                if (index <= _list.Count && index >= 0)
                    _list[index]=value;
                else
                    throw new IndexOutOfRangeException();
            }
        }

        public void Add(Student item)
        {
            _list.Add(item);
        }

        public void Clear()
        {
            _list.Clear();
        }

        public bool Contains(Student item)
        {
           return _list.Contains(item);
        }

        public void CopyTo(Student[] array, int arrayIndex)
        {
            _list.CopyTo(array,arrayIndex);
        }

        public int Count
        {
            get {
                return _list.Count();
            }
        }

        public bool IsReadOnly
        {
            get {
                return false;
            }
        }

        public bool Remove(Student item)
        {
            return _list.Remove(item);
        }

        public IEnumerator<Student> GetEnumerator()
        {
          return _list.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _list.GetEnumerator();
        }
      
    }
}
