﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Math_to_delegate
{
    class Program
    {
        static void Main(string[] args)
        {  double step,xStart,xEnd;
        step = 1;
        xStart = 0;
        xEnd = 30;
        int ii=(int)((xEnd - xStart) / step);

            //amount of functions that we want write
        int fNumber = 3;


        double[,] mas = new double[ii, fNumber+1];
        //send function and angles
       MathTable.CreateTable(Math.Cos,ref mas, xStart, xEnd, step,1);
       MathTable.CreateTable(Math.Sin, ref mas, xStart, xEnd, step, 2);
       MathTable.CreateTable(Math.Tan, ref mas, xStart, xEnd, step, 3);

       Console.WriteLine("angle" + "         " + "Cos" + "            " + "Sin" + "            " + "Tan");
       for (int i = 0; i < ii; i++)
           Console.WriteLine(mas[i, 0] + "            " + mas[i, 1] + "            " + mas[i, 2] + "            " + mas[i, 3]);



   
           Console.ReadKey();




        }
    }
}
