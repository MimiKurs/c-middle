﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Math_to_delegate
{
  static public class MathTable
    {

       public delegate double MyMathFunction(double varib);
      //rMunber- number of row that we want to fill
      static public void CreateTable(MyMathFunction futureFunc,ref double[,] mas, double xStart, double xEnd, double step,int rNumber)
       {
           double i = xStart;
           int j=0;
           while (i < xEnd)
           {
               mas[j, 0] = i;
               mas[j, rNumber] = Math.Round(futureFunc(Math.PI * i / 180.0),4);
               i += step;
                    j++;
           }

       }


    }
}
